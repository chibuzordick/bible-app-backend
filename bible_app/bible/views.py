from rest_framework import viewsets, views
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser
from .models import Sermon, Note, Image, Video
from .serializers import SermonSerializer
from .serializers import NoteSerializer
from .serializers import ImageSerializer
from .serializers import VideoSerializer
from .permissions import IsOwner, IsStaffOrReadOnly
from .serializers import SocialSignUpSerializer
from .permissions import IsAuthenticatedOrCreate
from rest_framework.response import Response
from rest_framework import status, generics
from social.apps.django_app.default.models import UserSocialAuth as User
from social_django.utils import load_strategy, load_backend
from social.backends.oauth import BaseOAuth1, BaseOAuth2
from social_core.exceptions import AuthAlreadyAssociated


class SermonViewSet(viewsets.ModelViewSet):
    serializer_class = SermonSerializer
    permission_classes = (IsAuthenticated, IsStaffOrReadOnly)
    queryset = Sermon.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class NoteViewSet(viewsets.ModelViewSet):
    serializer_class = NoteSerializer
    permission_classes = (IsAuthenticated, IsOwner)

    def get_queryset(self):
        """ Override default query sets to return only  user notes"""
        return Note.objects.filter(user=self.request.user.id)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ImageViewSet(viewsets.ViewSet):
    parser_classes = (MultiPartParser, FormParser)

    def create(self, request, file= None, format=None,pk=None):
        file_serializer = ImageSerializer(data=request.data, context={'request': request})

        if file_serializer.is_valid():
            file_serializer.save()

            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self, request):
        queryset = Image.objects.all()

        serializer = ImageSerializer(queryset, many=True, context={"request": request})
        return Response(serializer.data)

    def destroy(self, request, pk=None):

        try:
            img = Image.objects.get(pk=pk)
            if request.user.is_staff:
                img.delete()
        except Image.DoesNotExist as e:
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

        return Response("Deleted", status=status.HTTP_204_NO_CONTENT)

    def retrieve(self, request, pk=None):
        obj = Image.objects.filter(pk=pk).first()

        if obj is None:
            return Response("Not Found", status=status.HTTP_404_NOT_FOUND)

        serializer = ImageSerializer(obj)
        data = serializer.data
        return Response(data, status=status.HTTP_200_OK)


class SocialSignUpView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = SocialSignUpSerializer
    # This permission is nothing special, see part 2 of this series to see its entirety
    permission_classes = (IsAuthenticatedOrCreate,)

    def create(self, request, *args, **kwargs):
        """
        Override `create` instead of `perform_create` to access request
        request is necessary for `load_strategy`
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        provider = request.data['provider']

        # If this request was made with an authenticated user, try to associate this social
        # account with it
        authed_user = request.user if not request.user.is_anonymous else None

        # `strategy` is a python-social-auth concept referencing the Python framework to
        # be used (Django, Flask, etc.). By passing `request` to `load_strategy`, PSA
        # knows to use the Django strategy
        strategy = load_strategy(request)
        # Now we get the backend that corresponds to our user's social auth provider
        # e.g., Facebook, Twitter, etc.
        backend = load_backend(strategy=strategy, name=provider, redirect_uri=None)
        token = None
        if isinstance(backend, BaseOAuth1):
            # Twitter, for example, uses OAuth1 and requires that you also pass
            # an `oauth_token_secret` with your authentication request
            token = {
                'oauth_token': request.data['access_token'],
                'oauth_token_secret': request.data['access_token_secret'],
            }
        elif isinstance(backend, BaseOAuth2):
            # We're using oauth's implicit grant type (usually used for web and mobile
            # applications), so all we have to pass here is an access_token
            token = request.data['access_token']

        try:
            # if `authed_user` is None, python-social-auth will make a new user,
            # else this social account will be associated with the user you pass in
            user = backend.do_auth(token, user=authed_user)
        except AuthAlreadyAssociated:
            # You can't associate a social account with more than user
            return Response({"errors": "That social media account is already in use"},
                            status=status.HTTP_400_BAD_REQUEST)

        if user and user.is_active:
            # if the access token was set to an empty string, then save the access token
            # from the request
            auth_created = user.social_auth.get(provider=provider)
            if not auth_created.extra_data['access_token']:
                # Facebook for example will return the access_token in its response to you.
                # This access_token is then saved for your future use. However, others
                # e.g., Instagram do not respond with the access_token that you just
                # provided. We save it here so it can be used to make subsequent calls.
                auth_created.extra_data['access_token'] = token
                auth_created.save()

            # Set instance since we are not calling `serializer.save()`
            serializer.instance = user
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)
        else:
            return Response({"errors": "Error with social authentication"},
                            status=status.HTTP_400_BAD_REQUEST)

# class Ima(viewsets.ModelViewSet):
#     serializer_class = ImageSerializer
#     permission_classes = (IsAuthenticatedOrReadOnly,)
#     queryset = Image.objects.all()
#     parser_class = (MultiPartParser, FormParser)
#
#     def perform_create(self, serializer):
#
#         file_serializer = ImageSerializer(data=self.request.data, context={'request': self.request})
#
#         if file_serializer.is_valid():
#             file_serializer.save()
#
#             return Response(file_serializer.data, status=status.HTTP_201_CREATED)
#         else:
#             return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VideoViewSet(viewsets.ViewSet):
    parser_classes = (MultiPartParser, FormParser)

    def create(self, request, file=None, format=None, pk=None):
        file_serializer = VideoSerializer(data=request.data, context={'request': request})

        if file_serializer.is_valid():
            file_serializer.save()

            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self, request):
        queryset = Video.objects.all()

        serializer = VideoSerializer(queryset, many=True, context={"request": request})
        return Response(serializer.data)

    def destroy(self, request, pk=None):

        try:
            vid = Video.objects.get(pk=pk)
            if request.user.is_staff:
                vid.delete()
        except Video.DoesNotExist as e:
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

        return Response("Deleted", status=status.HTTP_204_NO_CONTENT)

    def retrieve(self, request, pk=None):
        obj = Video.objects.filter(pk=pk).first()

        if obj is None:
            return Response("Not Found", status=status.HTTP_404_NOT_FOUND)

        serializer = VideoSerializer(obj)
        data = serializer.data
        return Response(data, status=status.HTTP_200_OK)