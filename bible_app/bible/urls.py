from django.urls import include, path
from rest_framework import routers
from . import views
from django.conf.urls import url
from .views import SocialSignUpView

app_name = 'api'

router = routers.DefaultRouter()
router.register(r'sermons', views.SermonViewSet, base_name='sermons')
router.register(r'notes', views.NoteViewSet, base_name='notes')
router.register(r'images', views.ImageViewSet, base_name='images')
router.register(r'videos', views.VideoViewSet, base_name='videos')

urlpatterns = [
    path('', include(router.urls)),
    url(r'^sign_up/', SocialSignUpView.as_view(), name='sign_up_login'),
]
