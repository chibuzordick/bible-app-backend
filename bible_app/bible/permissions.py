from rest_framework.permissions import BasePermission, SAFE_METHODS, IsAuthenticated


class IsOwner(BasePermission):
    """
    Permission to only allow users of object to only action on the
    object
    """

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class IsStaffOrReadOnly(BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if not request.user.is_staff and request.method not in SAFE_METHODS:
            return False
        return True

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        if request.method in SAFE_METHODS:

            return True
        return request.user.is_staff and obj.user == request.user


class IsAuthenticatedOrCreate(IsAuthenticated):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        return super(IsAuthenticatedOrCreate, self).has_permission(request, view)