from rest_framework import serializers
from taggit_serializer.serializers import (TagListSerializerField,
                                           TaggitSerializer)
from .models import Sermon
from .models import Image
from .models import Note
from .models import Sermon
from .models import Video

from django.contrib.auth import get_user_model
from social.apps.django_app.default.models import UserSocialAuth
from rest_framework import serializers


User = get_user_model()


class SignUpSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password')
        write_only_fields = ('password',)


class SocialSignUpSerializer(SignUpSerializer):

    class Meta(SignUpSerializer.Meta):
        model = UserSocialAuth
        fields = ('provider', 'access_token',)
        read_only_fields = ('password',)

        access_token_secret = serializers.CharField(required=False, allow_blank=True)


class ImageSerializer(serializers.ModelSerializer):

    link =  serializers.SerializerMethodField()
    id = serializers.IntegerField(required=False)
    key = serializers.IntegerField(required=False)
    file = serializers.FileField(use_url=True, required=False)

    class Meta:
        model = Image
        fields = ('id', 'link', 'file','key',
                  'created_date', 'modified_date')
        read_only_fields = ('id',)

    def get_link(self, instance):
        request = self.context.get('request')


        return request.build_absolute_uri(instance.file.url)

class VideoSerializer(serializers.ModelSerializer):
    link = serializers.SerializerMethodField()
    id = serializers.IntegerField(required=False)
    key = serializers.IntegerField(required=False)
    file = serializers.FileField(use_url=True, required=False)
    class Meta:
        model = Video
        fields = ('id', 'link', 'key', 'file',
                  'created_date', 'modified_date')

    def get_link(self, instance):
        request = self.context.get('request')

        return request.build_absolute_uri(instance.file.url)

class SermonSerializer(TaggitSerializer,serializers.ModelSerializer):

    title = serializers.CharField(min_length=15, max_length=250)
    content = serializers.CharField(min_length=20, required=True)
    pastor = serializers.StringRelatedField(source='user', read_only=True)
    images = ImageSerializer(many=True, required=False)
    #images = serializers.PrimaryKeyRelatedField(many=True, read_only=False,  required=False, queryset=Image.objects.all())
    videos = VideoSerializer(many=True, required=False)
    tags = TagListSerializerField(required=False)

    class Meta:
        model = Sermon
        fields = ('id', 'title', 'content', 'pastor', 'user', 'tags', 'images', 'videos',
                  'created_date', 'schedule_date', 'modified_date')
        read_only_fields = ('id',)

    def create(self, validated_data):
        images = validated_data.pop('images', [])
        videos = validated_data.pop('videos', [])
        sermon = super(SermonSerializer, self).create(validated_data)
        for image in images:

            sermon.images.add(image['id'])
        for vid in videos:

            sermon.videos.add(vid['id'])
        return sermon

    def update(self, instance, validated_data):
        images_data = validated_data.pop('images', [])
        #images_data = [x['id'] for x in images_data]
        videos_data = validated_data.pop('videos', [])
        sermon = super(SermonSerializer, self).update(instance, validated_data)
        # delete old
        #sermon.objects.exclude(images_id__in=images_data).delete()
        # create new
        for image in images_data:
            sermon.images.add(image['id'])
        for vid in videos_data:

            sermon.videos.add(vid['id'])
        return sermon

class NoteSerializer(TaggitSerializer,serializers.ModelSerializer):

    title = serializers.CharField(min_length=10, max_length=250)
    content = serializers.CharField(min_length=20, required=False)
    sermon = serializers.PrimaryKeyRelatedField( queryset=Sermon.objects.all())
    tags = TagListSerializerField(required=False)


    class Meta:
        model = Note
        fields = ('id', 'title', 'content',  'tags', 'sermon',
                  'created_date', 'modified_date')
        read_only_fields = ('id',)
