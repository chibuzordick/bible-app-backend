from PIL import Image
import os
from django.conf import settings
import tempfile
from rest_framework.test import APITestCase, APIClient
from django.contrib.auth.models import User
from rest_framework import status
from django.urls import reverse
from .models import Sermon, Note
from .serializers import SermonSerializer, NoteSerializer
from .views import ImageViewSet

class BaseTest(APITestCase):
    def setup_demo_user(self):
        """Helper method to create a user and log in the user"""
        self.pastor_user = User.objects.create_user('pastor_user',is_staff=True)
        self.second_pastor_user = User.objects.create_user('_second pastor_user',is_staff=True)
        self.normal_user = User.objects.create_user('normal_user')
        self.unauth_client = APIClient()
        self.normal_client = APIClient()
        self.second_pastor_client = APIClient()

        self.normal_client.force_login(self.normal_user)
        self.pastor_client = self.client
        self.client.force_login(self.pastor_user)
        self.second_pastor_client.force_login(self.second_pastor_user)
        self.bible_list_url = reverse("api:sermons-list")
        self.notelist_url = reverse("api:notes-list")
        self.images_url = reverse("api:images-list", )
        self.videos_url = reverse("api:videos-list", )

class APITest(BaseTest):
    def  _create_sermon(self, pastor_user):
        data = self.sermon_data.copy()
        data['user'] = pastor_user
        sermon = Sermon(**data)
        sermon.save()
        return sermon



    def setUp(self):
        self.setup_demo_user()
        self.sermon_data = {'title': "Breaking from enemy chains",'content': "Receive Your healing today"}
        self.note_data = {'title':'Ride On Pastor'}
        sermon = Sermon(**self.sermon_data)
        sermon.save()
        self.note_data_with_sermon = {'title':'Ride On Pastor', 'sermon': sermon.pk}

    def test_cannot_create_sermon_without_auth(self):

        response = self.unauth_client.post(self.bible_list_url, self.sermon_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_staff_pastor_can_create_sermon(self):
        response = self.pastor_client.post(self.bible_list_url, self.sermon_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    def test_staff_pastor_can_get_sermon(self):
        data = self.sermon_data.copy()
        sermon = Sermon(**data)
        sermon.save()
        sermons = Sermon.objects.all()
        sermon_ser = SermonSerializer(sermons, many=True)
        response = self.pastor_client.get(self.bible_list_url)
        self.assertEqual(response.data, sermon_ser.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_staff_pastor_cannot_action_other_sermon(self):
        sermon = self._create_sermon(self.second_pastor_user)
        url = reverse("api:sermons-detail", kwargs={'pk': sermon.pk})
        response = self.pastor_client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        sermon_data = {'title': 'Coming back of christ'}
        response = self.pastor_client.patch(url, sermon_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.pastor_client.put(url, self.sermon_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.pastor_client.patch(url, sermon_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.pastor_client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_cannot_create_create_sermon(self):

        self.normal_client.force_login(self.normal_user)
        response = self.normal_client.post(self.bible_list_url, self.sermon_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_cannot_edit_sermon(self):
        sermon = self._create_sermon(self.second_pastor_user)
        url = reverse("api:sermons-detail", kwargs={'pk': sermon.pk})


        sermon_data = {'title': 'Coming back of christ'}
        response = self.normal_client.patch(url, sermon_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.normal_client.put(url, self.sermon_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.normal_client.patch(url, sermon_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.normal_client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_cannot_del_sermon(self):
        sermon = self._create_sermon(self.pastor_user)
        url = reverse("api:sermons-detail", kwargs={'pk': sermon.pk})
        response = self.normal_client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_pastor_can_action_sermon(self):
        sermon = self._create_sermon(self.pastor_user)
        url = reverse("api:sermons-detail", kwargs={'pk': sermon.pk})

        sermon_data = {'title': 'Coming back of christ'}
        response = self.pastor_client.patch(url, sermon_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.pastor_client.put(url, self.sermon_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.pastor_client.patch(url, sermon_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.pastor_client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)



    def test_pastor_can_del_sermon(self):
        sermon = self._create_sermon(self.pastor_user)
        url = reverse("api:sermons-detail", kwargs={'pk': sermon.pk})
        response = self.pastor_client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_can_create_sermon_note(self):
        self.normal_client.force_login(self.normal_user)
        response = self.normal_client.post(self.notelist_url, self.note_data_with_sermon, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_get_sermon_note(self):

        #response =  self.normal_client.post(self.notelist_url, self.note_data_with_sermon, format='json')
        data = self.note_data_with_sermon.copy()
        data['user_id'] = self.normal_user.id
        data['sermon'] = Sermon.objects.get(pk=data['sermon'])
        note = Note(**data)
        note.save()
        notes = Note.objects.filter(user=self.normal_user.id)
        note_ser = NoteSerializer(notes, many=True)
        response = self.normal_client.get(self.notelist_url)
        self.assertEqual(response.data, note_ser.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_action_sermon_note(self):
        response =  self.normal_client.post(self.notelist_url, self.note_data_with_sermon, format='json')
        r_id = response.data['id']
        url = reverse("api:notes-detail", kwargs={'pk': r_id})
        note_data = {'title': 'Coming back of christ'}
        response = self.normal_client.patch(url, note_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        note_data['sermon'] = response.data['sermon']
        response = self.normal_client.put(url, note_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.normal_client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_user_can_del_sermon_note(self):
        response = self.normal_client.post(self.notelist_url, self.note_data_with_sermon, format='json')
        r_id = response.data['id']
        url = reverse("api:notes-detail", kwargs={'pk': r_id})
        response = self.normal_client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_cannot_create_sermon_note_without_auth(self):
        response = self.unauth_client.post(self.notelist_url, self.note_data_with_sermon, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_cannot_action_other_sermon_note(self):
        response = self.pastor_client.post(self.notelist_url, self.note_data_with_sermon, format='json')
        r_id = response.data['id']
        url = reverse("api:notes-detail", kwargs={'pk': r_id})
        response = self.normal_client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        note_data = {'title': 'Coming back of christ'}
        response = self.normal_client.patch(url, note_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND )
        response = self.normal_client.put(url, self.sermon_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.normal_client.patch(url, note_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.normal_client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_can_create_sermon_with_images_videos(self):
        image = Image.new('RGB', (100, 100))

        tmp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
        image.save(tmp_file)
        tmp_file.seek(0)
        images = []
        response = self.client.post(self.images_url, {'file': tmp_file}, format='multipart')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        images.append({'id':response.data['id']})
        tmp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
        image.save(tmp_file)
        tmp_file.seek(0)
        response = self.client.post(self.images_url, {'file': tmp_file}, format='multipart')
        images.append({'id':response.data['id']})
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        response = self.client.get(self.images_url,   format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        data = self.sermon_data.copy()
        data['images'] = images
        response = self.pastor_client.post(self.bible_list_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        url = reverse("api:sermons-detail", kwargs={'pk': response.data['id']})
        response = self.pastor_client.get(url,   format='json')
        ret_img = [x['id'] for x in response.data['images']]
        data_img = [x['id'] for x in images]
        self.assertEqual(set(data_img), set(ret_img))
        urlim = reverse("api:images-detail", kwargs={'pk':  ret_img[0]})

        response = self.pastor_client.delete(urlim, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


    def test_can_create_sermon_with_tags(self):
        data = self.sermon_data.copy()
        data['tags'] = ['pray','good']
        response = self.pastor_client.post(self.bible_list_url, data, format='json')
        self.assertEqual(set(response.data['tags']),set( data['tags']))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_sermon_contains_images_videos(self):
        gif_file = os.path.join(settings.MEDIA_ROOT, '1490978656.gif')
        with open(gif_file, 'rb') as fl:
            images = []
            response = self.client.post(self.videos_url, {'file': fl}, format='multipart')
            self.assertEqual(status.HTTP_201_CREATED, response.status_code)
            images.append({'id': response.data['id']})

            response = self.client.get(self.videos_url, format='json')
            self.assertEqual(status.HTTP_200_OK, response.status_code)
            data = self.sermon_data.copy()
            data['videos'] = images
            response = self.pastor_client.post(self.bible_list_url, data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            url = reverse("api:sermons-detail", kwargs={'pk': response.data['id']})
            response = self.pastor_client.get(url, format='json')
            ret_img = [x['id'] for x in response.data['videos']]
            data_img = [x['id'] for x in images]
            self.assertEqual(set(data_img), set(ret_img))
            urlim = reverse("api:videos-detail", kwargs={'pk': ret_img[0]})
            response = self.pastor_client.delete(urlim, format='json')
            self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)



    def test_can_create_note_with_tags(self):
        data = self.note_data_with_sermon.copy()
        data['tags'] = ['pray', 'good']
        response = self.normal_client.post(self.notelist_url, data, format='json')

        self.assertEqual(set(response.data['tags']), set(data['tags']))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
