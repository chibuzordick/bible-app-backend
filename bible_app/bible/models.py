from django.db import models
from django.contrib.auth.models import User

from taggit.managers import TaggableManager

class Image(models.Model):
    link = models.URLField(blank=True, null=True)
    key = models.PositiveIntegerField(blank=True, null=True)
    file = models.FileField(blank=False, null=False)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.file.name


class Video(models.Model):
    link = models.URLField(blank=True, null=True)
    key = models.PositiveIntegerField(blank=True, null=True)
    file = models.FileField(blank=False, null=False)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.file.name

class Sermon(models.Model):
    """sermon models for pastor"""

    title =  models.CharField(max_length=250, db_index=True)
    bible_verse =  models.CharField(max_length=250, null=True, blank=True)
    content = models.TextField( )
    user = models.ForeignKey(User, on_delete=models.SET_NULL,  null=True, verbose_name='Pastor')
    tags = TaggableManager(  blank=True)
    images = models.ManyToManyField(Image, related_name='sermons' , blank=True)
    videos = models.ManyToManyField(Video, related_name='sermons',   blank=True)
    schedule_date = models.DateTimeField(auto_now_add=True, help_text='Date which sermon is going to take place')
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    class Meta:
        ordering = ['-schedule_date']
        verbose_name = "sermon"
        verbose_name_plural = "sermons"

class Note(models.Model):

    title = models.CharField(max_length=250, db_index=True)
    content = models.TextField()
    tags = TaggableManager()
    sermon = models.ForeignKey(Sermon, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

